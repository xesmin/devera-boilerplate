import mongoose from 'mongoose'

const UserModel = new mongoose.Schema({
  username: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
    select: false,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  firstName: {
    type: String,
    required: false,
    default: '',
  },
  lastName: {
    type: String,
    reqired: false,
    default: '',
  },
  phone: {
    type: String,
    required: false,
    default: '',
  },
  roles: {
    type: Array,
    default: [],
  },
})

export default mongoose.model('User', UserModel)
