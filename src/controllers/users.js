import bcrypt from 'bcrypt'
import User from '../models/User'

class UserController {
  static async checkForConflicts(userData, id) {
    const emailExist = await User.findOne({ email: userData.email })
    const usernameExist = await User.findOne({ username: userData.username })

    if (!id) {
      if (emailExist) {
        throw { status: 409, error: 'Email is taken' }
      }

      if (usernameExist) {
        throw { status: 409, error: 'Username is taken' }
      }
    } else {
      if (emailExist && emailExist._id != id) {
        throw { status: 409, error: 'Email is taken' }
      }

      if (usernameExist && usernameExist._id != id) {
        throw { status: 409, error: 'Username is taken' }
      }
    }
  }

  /**
   * Create new user in database.
   * @param {Object} userData Object containing new user data.
   * @param {string} userData.username
   * @param {string} userData.password
   * @param {string} userData.email
   * @param {string} [userData.firstName]
   * @param {string} [userData.lastName]
   * @param {string} [userData.phone]
   *
   * @returns {Promise<Object>} New user object.
   */
  static async create(userData) {
    try {
      await this.checkForConflicts(userData)

      const password = await bcrypt.hash(userData.password, 10)
      const newUser = await User.create({ ...userData, password })

      newUser.password = undefined
      newUser.__v = undefined

      return newUser
    } catch (err) {
      throw err
    }
  }

  /**
   * Removes user from database.
   * @param {number} id Id of user to remove.
   * @returns {Promise<Object>}
   */
  static async remove(id) {
    try {
      return await User.findOneAndRemove({ _id: id })
    } catch (err) {
      throw err
    }
  }

  /**
   * Get all users from database.
   * @returns {Promise<Array>} Objects of all users data.
   */
  static async getAll() {
    try {
      return await User.find({})
    } catch (err) {
      throw err
    }
  }

  /**
   * Get single user from database.
   * @param {string} id Id of user to get.
   * @returns {Promise<Object>} User data object.
   */
  static async get(id) {
    try {
      return await User.findById(id)
    } catch (err) {
      throw err
    }
  }

  /**
   * Update user in database.
   * @param {Object} userData Object containing new user data.
   * @param {string} [userData.username]
   * @param {string} [userData.password]
   * @param {string} [userData.email]
   * @param {string} [userData.firstName]
   * @param {string} [userData.lastName]
   * @param {string} [userData.phone]
   *
   * @returns {Promise<Object>} Updated user object.
   */
  static async update(id, userData) {
    try {
      const user = await User.findById(id)

      const updatedUserData = { ...userData }

      if (updatedUserData.password) {
        updatedUserData.password = await bcrypt.hash(
          updatedUserData.password,
          10,
        )
      }

      if (!user) {
        throw { status: 404, error: 'Not Found' }
      }

      await this.checkForConflicts(updatedUserData, id)

      return await User.findByIdAndUpdate(id, updatedUserData, { new: true })
    } catch (err) {
      throw err
    }
  }

  /**
   * Check login data and return user object if login data is valid.
   * @param {string} login Email or username of user.
   * @param {string} password Password of user.
   * @returns {Promise<Object>} Logged in user data.
   */
  static async checkLoginData(login, password) {
    try {
      const user = await User.findOne({
        $or: [{ email: login }, { username: login }],
      }).select('+password')

      if (!user) {
        throw { status: 401, error: 'Unauthorized' }
      }

      const validPassword = await bcrypt.compare(password, user.password)

      if (!validPassword) {
        throw { status: 401, error: 'Unauthorized' }
      }

      user.password = undefined

      return user
    } catch (err) {
      throw err
    }
  }
}

export default UserController
