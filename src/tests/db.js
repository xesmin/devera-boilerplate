import mongoose from 'mongoose'
import { MongoMemoryServer } from 'mongodb-memory-server'

let mongod

export const connect = async () => {
  mongod = await MongoMemoryServer.create()
  const uri = await mongod.getUri()

  const opts = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
  }

  await mongoose.connect(uri, opts)
}

export const closeDatabase = async () => {
  await mongoose.connection.dropDatabase()
  await mongoose.connection.close()
  await mongod.stop()
}

export const clearDatabase = async () => {
  const collections = mongoose.connection.collections
  for (const key in collections) {
    const collection = collections[key]
    await collection.deleteMany()
  }
}

export const objectId = () => {
  return mongoose.Types.ObjectId()
}

export default {
  connect,
  closeDatabase,
  clearDatabase,
  objectId,
}
