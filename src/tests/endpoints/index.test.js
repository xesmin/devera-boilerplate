import app from '../../index'
import db from '../db'
import supertest from 'supertest'

const request = supertest(app)

beforeAll(async () => await db.connect())
afterAll(async () => await db.closeDatabase())

describe('System routes', () => {
  test('GET /', async () => {
    const res = await request.get('/')

    expect(res.status).toBe(200)
    expect(res.body.message).toBe('Online')
  })
})
