import app from '../../index'
import db from '../db'
import supertest from 'supertest'

const request = supertest(app)

beforeAll(async () => await db.connect())
afterAll(async () => await db.closeDatabase())

describe('Users endpoints', () => {
  const userData = {
    username: 'TestUser',
    email: 'test@test.com',
    password: 'test1234',
    repeat_password: 'test1234',
    firstName: 'Tester',
    lastName: 'Testing',
    phone: '12345678',
  }

  test('POST /users', async () => {
    const res = await request.post('/users').send(userData)

    expect(res.status).toBe(201)
    expect(res.body.email).toBe(userData.email)
    expect(res.body).toHaveProperty('_id')

    userData._id = res.body._id
  })

  test('GET /users', async () => {
    const res = await request.get('/users')

    expect(res.body).toHaveLength(1)
  })

  test('GET /users/:id', async () => {
    const res = await request.get(`/users/${userData._id}`)

    expect(res.body._id).toBe(userData._id)
  })

  test('PATCH /users/:id', async () => {
    const res = await request.patch(`/users/${userData._id}`).send({
      firstName: 'Retset',
    })

    expect(res.status).toBe(200)
    expect(res.body.firstName).toBe('Retset')
    expect(res.body._id).toBe(userData._id)
  })

  test('DELETE /users/:id', async () => {
    const res = await request.delete(`/users/${userData._id}`)

    expect(res.status).toBe(204)

    const res2 = await request.get('/users')

    expect(res2.body).toHaveLength(0)
  })
})
