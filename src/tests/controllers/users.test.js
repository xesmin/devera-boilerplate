import db from '../db'
import UserController from '../../controllers/users'

beforeAll(async () => await db.connect())
afterAll(async () => await db.closeDatabase())

describe('Users Controller', () => {
  const userData = {
    username: 'TestUser',
    email: 'test@test.com',
    password: 'test1234',
    repeat_password: 'test1234',
    firstName: 'Tester',
    lastName: 'Testing',
    phone: '12345678',
    _id: db.objectId(),
  }

  test('Create new user', async () => {
    const newUser = await UserController.create(userData)

    expect(newUser._id).toEqual(userData._id)
  })

  test('Get user', async () => {
    const user = await UserController.get(userData._id)

    expect(user.email).toEqual(userData.email)
  })

  test('Get all users', async () => {
    const users = await UserController.getAll()

    expect(users.length).toEqual(1)
  })

  test('Update user', async () => {
    const updatedUser = await UserController.update(userData._id, {
      firstName: 'Retset',
    })

    expect(updatedUser._id).toStrictEqual(userData._id)
    expect(updatedUser.firstName).toBe('Retset')
  })

  test('Check login with valid password', async () => {
    const loggedInUser = await UserController.checkLoginData(
      userData.username,
      userData.password,
    )

    expect(loggedInUser._id).toStrictEqual(userData._id)
    expect(loggedInUser.password).toBe(undefined)
  })

  test('Check login with invalid password', async () => {
    try {
      const validPassword = await UserController.checkLoginData(
        userData.username,
        'wrongpassword',
      )
    } catch (error) {
      expect(error).toStrictEqual({ status: 401, error: 'Unauthorized' })
    }
  })

  test('Remove user', async () => {
    await UserController.remove(userData._id)
    const user = await UserController.get(userData._id)
    const users = await UserController.getAll()

    expect(user).toBe(null)
    expect(users).toHaveLength(0)
  })
})
