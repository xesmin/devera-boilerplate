import './utils/config'
import './utils/database'

import chalk from 'chalk'
import cookieParser from 'cookie-parser'
import cors from 'cors'
import express from 'express'
import helmet from 'helmet'
import passport from 'passport'

import UsersRouter from './routes/users'
import AuthRouter from './routes/auth'

// Init express app
const app = express()
app.use(express.json())
app.use(helmet())
app.use(cookieParser())
app.use(cors())
app.use(passport.initialize())
import './utils/passport'

/* SYSTEM ROUTES */
app.get('/', async (req, res, next) =>
  res.status(200).json({ message: 'Online' }),
)

/* API ROUTES */
app.use('/users', UsersRouter)
app.use('/auth', AuthRouter)

/* ERROR HANDLING */
app.all('*', async (req, res, next) => {
  next({ status: 404, error: 'Not Found' })
})

app.use(async (err, req, res, next) => {
  res
    .status(err.status || 500)
    .json({ status: err.status, error: err.error, handled: true })
})

// Start app
const PORT = process.env.PORT || 3000
if (require.main === module) {
  app.listen(PORT, () => {
    console.log(`🚀 ${chalk.greenBright(`App listening on port ${PORT}...`)}`)
  })
}

export default app
