import passport from 'passport'
import { Strategy as JWTStrategy } from 'passport-jwt'
import UserController from '../controllers/users'

const cookieExtractor = req => {
  let token = null
  if (req && req.cookies) {
    token = req.cookies['jwt']
  }
  return token
}

passport.use(
  new JWTStrategy(
    {
      secretOrKey: process.env.JWT_SECRET,
      jwtFromRequest: cookieExtractor,
    },
    async (payload, done) => {
      try {
        console.log(payload._id)
        const user = await UserController.get(payload._id)

        return user ? done(null, user) : done(null, false)
      } catch (err) {
        return err, false
      }
    },
  ),
)
