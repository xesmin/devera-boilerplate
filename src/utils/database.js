import mongoose from 'mongoose'
import chalk from 'chalk'
if (process.env.NODE_ENV !== 'test') {
  const connection = mongoose.connect(process.env.MONGODB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
  })

  mongoose.connection.on('connected', () =>
    console.log(chalk.greenBright('🚀 Connected to database...')),
  )
}
