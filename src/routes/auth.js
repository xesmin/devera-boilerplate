import express from 'express'
import JWT from 'jsonwebtoken'
import UserController from '../controllers/users'
import { validateBody, SCHEMAS } from '../middleware/validators'
import { isLoggedIn } from '../middleware/access'

const router = express.Router()

router.post(
  '/login',
  validateBody(SCHEMAS.BODY.POST_AUTH_LOGIN),
  async (req, res, next) => {
    try {
      const user = await UserController.checkLoginData(
        req.body.username || req.body.email,
        req.body.password,
      )

      if (!user) {
        return next({ status: 401, error: 'Unauthorized' })
      }

      const token = await JWT.sign(
        JSON.stringify({
          _id: user._id,
        }),
        process.env.JWT_SECRET,
      )

      res.cookie('jwt', token, {
        httpOnly: true,
      })

      return res.status(200).json(user)
    } catch (error) {
      return next(error)
    }
  },
)

router.get('/logout', isLoggedIn, async (req, res, next) => {
  res.clearCookie('jwt')

  return res.status(204).end()
})

router.get('/me', isLoggedIn, async (req, res, next) => {
  return res.status(200).json(req.user)
})

export default router
