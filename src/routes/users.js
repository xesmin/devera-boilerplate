import express from 'express'
import UserController from '../controllers/users'
import {
  validateBody,
  validateParamId,
  SCHEMAS,
} from '../middleware/validators'

const router = express.Router()

router.get('/', async (req, res, next) => {
  try {
    const users = await UserController.getAll()

    return res.status(200).json(users)
  } catch (error) {
    return next({ error })
  }
})

router.get('/:id', validateParamId, async (req, res, next) => {
  try {
    const user = await UserController.get(req.params.id)

    if (!user) {
      return next({ status: 404 })
    }

    return res.status(200).json(user)
  } catch (error) {
    return next({ error })
  }
})

router.post(
  '/',
  validateBody(SCHEMAS.BODY.POST_USERS),
  async (req, res, next) => {
    try {
      const newUser = await UserController.create(req.body)

      return res.status(201).json(newUser)
    } catch (error) {
      return next({ error })
    }
  },
)

router.delete('/:id', validateParamId, async (req, res, next) => {
  try {
    await UserController.remove(req.params.id)

    return res.status(204).end()
  } catch (error) {
    return next({ error })
  }
})

router.patch(
  '/:id',
  validateParamId,
  validateBody(SCHEMAS.BODY.PATCH_USERS),
  async (req, res, next) => {
    try {
      const updatedUser = await UserController.update(req.params.id, req.body)

      return res.status(200).json(updatedUser)
    } catch (error) {
      return next({ error })
    }
  },
)

export default router
