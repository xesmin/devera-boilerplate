import getUsersParams from './validation/getUsersParams'
import objectId from './validation/objectId'
import patchUsers from './validation/patchUsers'
import postAuthLogin from './validation/postAuthLogin'
import postUsers from './validation/postUsers'

export const SCHEMAS = {
  BODY: {
    PATCH_USERS: 'patchUsers',
    POST_AUTH_LOGIN: 'postAuthLogin',
    POST_USERS: 'postUsers',
  },
  PARAMS: {
    GET_USERS: 'getUsersParams',
  },
}

const _SCHEMAS = {
  [SCHEMAS.BODY.PATCH_USERS]: patchUsers,
  [SCHEMAS.BODY.POST_AUTH_LOGIN]: postAuthLogin,
  [SCHEMAS.BODY.POST_USERS]: postUsers,
  [SCHEMAS.PARAMS.GET_USERS]: getUsersParams,
}

export const validateBody = schema => async (req, res, next) => {
  try {
    await _SCHEMAS[schema].validateAsync(req.body)

    return next()
  } catch (error) {
    return next({ status: 400, error })
  }
}

export const validateParams = schema => async (req, res, next) => {
  try {
    await _SCHEMAS[schema].validateAsync(req.params)

    return next()
  } catch (error) {
    return next({ status: 400, error })
  }
}

export const validateQuery = schema => async (req, res, next) => {
  try {
    await _SCHEMAS[schema].validateAsync(req.query)

    return next()
  } catch (error) {
    return next({ status: 400, error })
  }
}

export const validateParamId = async (req, res, next) => {
  try {
    await objectId.validateAsync(req.params)

    return next()
  } catch (error) {
    return next({ status: 400, error })
  }
}
