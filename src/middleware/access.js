import passport from 'passport'

export const isLoggedIn = passport.authenticate('jwt', { session: false })

export const hasRole = role => (req, res, next) => {
  if (!req.user) {
    return next({ staus: 401, error: 'Unauthorized' })
  }

  const hasThisRole = !!req.user.roles?.find(el => el === role)

  if (!hasThisRole) {
    return next({ staus: 403, error: 'Forbidden' })
  }

  return next()
}
