import joi from 'joi'

export default joi
  .object({
    email: joi.string().email({ minDomainSegments: 2 }),
    password: joi.string().min(8).max(30).required(),
    username: joi.string().alphanum().min(3).max(30),
  })
  .xor('username', 'email')
