import joi from 'joi'
import joid from 'joi-objectid'

joi.objectId = joid(joi)

export default joi.object({
  id: joi.objectId(),
})
