import joi from 'joi'

export default joi
  .object({
    email: joi.string().email({ minDomainSegments: 2 }),
    firstName: joi.string().min(2).max(20),
    lastName: joi.string().min(2).max(20),
    password: joi.string().min(8).max(30),
    phone: joi.string().min(7).max(12),
    repeat_password: joi.ref('password'),
    username: joi.string().alphanum().min(3).max(30),
  })
  .with('password', 'repeat_password')
