module.exports = {
  root: true,
  env: {
    node: true,
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
  extends: ['plugin:prettier/recommended'],
  // add your custom rules here
  rules: {
    'no-nested-ternary': 2,
  },
}
