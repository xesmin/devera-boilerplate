# Devera Boilerplate

## Getting started

* If you don't want to use Docker:

```bash
# You need to setup mongodb by yourself first
cp .env.sample .env
yarn
yarn dev
```

* If you use docker

```bash
docker-compose up -d
```