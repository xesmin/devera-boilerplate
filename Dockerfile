FROM node:alpine
WORKDIR /app
COPY .babelrc .babelrc
COPY package.json package.json
RUN yarn
CMD ["yarn", "dev"]